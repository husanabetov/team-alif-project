import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
//import store from "./store";
import VueCookies from 'vue3-cookies';

const app = createApp(App)
router.app = app;
app.use(router).use(VueCookies).mount("#app");
