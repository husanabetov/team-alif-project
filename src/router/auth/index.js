// import { createWebHistory, createRouter } from "vue-router";
//import AuthLayout from '../layout/AuthLayout';
//import Home from '../views/app/Home';
import AuthLayout from '../../layout/AuthLayout';
import Home from '../../views/app/Home'

const routes = [
  {
    path: "/login",
    name: "Login",
    component: AuthLayout,
  },
  {
      path:"/home",
      name:"Home",
      component:Home
  }
];

export default routes;