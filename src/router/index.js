// import Vue from 'vue';
import {createRouter, createWebHistory} from 'vue-router';
import auth from './auth';
import app from './app';

// Vue.use(VueRouter);
const router = createRouter({
  history: createWebHistory(),
  routes: [...auth, ...app],
});

router.beforeEach((to, from, next) => {
 const{$cookies}=router.app.config.globalProperties
  console.log(router.app.config); 
  const loggedIn = $cookies.get('token');
  console.log(loggedIn);
  if (to.matched.some(record => record.meta.requiresAuth) && !loggedIn) {
    next({name:"Login"})
  }
  next();
})

export default router;