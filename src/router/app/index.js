// import { createWebHistory, createRouter } from "vue-router";
import AppLayout from '../../layout/AppLayout';
import AuthLayout from '../../layout/AuthLayout'
const routes = [
  {
    path: "/",
    name: "AppLayout",
    component: AppLayout,
    meta: { requiresAuth: true },
    children: [
      {
        path: '/',
        name:"AuthLayout",
        component:AuthLayout
      }
      // {
      //   path: 'employee-info',
      //   component: () => import('@/views/employees/EmployeesIndex.vue')
      // },
      // {
      //   p
      // }
    ]
  }
];


export default routes;