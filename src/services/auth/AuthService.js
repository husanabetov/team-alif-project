import { apiService } from '../index'

export const authService = {
    login(params) {
        return apiService.post('auth/login', params)
    }
}